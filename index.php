<!doctype html>
<html>
<head>
<?php 
require 'Import.php';
?>
<meta charset="UTF-8"/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 
 <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
</head>
<body>
<div class="container">
<h1>Import CSV</h1>
<h3>#zadanie 2</h3>
<div class="uploader">
<form action="sendData.php"  method="POST" enctype="multipart/form-data">
  <div class="form-group">
    <label for="exampleFormControlFile1">Example file input</label>
    <input type="file" class="form-control form-control-file" name="filename" id="exampleFormControlFile1">
    <input type="submit" name="upload" class="btn btn-primary" />
  </div>

</form>
</div>
<h2>Ilosc osob z kraju</h2>
<?php
$import = new Import();
    ?>
<canvas id="myChart"></canvas>

<table class="table table-hover">
<thead>
<th>#</th>
<th>Ilosc</th>
<th>Kraj</th>
</thead>
<?php 
try {
    foreach (json_decode($import->getData(), true) as $key => $row) {
        ?>
<tr>
<td>
<?php
echo $key + 1; ?>
</td>
<td>
<?php 
echo $row['qty']; ?>
</td>
<td>
<?php 
echo $row['country']; ?>

</td>
</tr>
<?php
    }
} catch (Exception $e) {
    echo '<div class="alert alert-danger">Brak pliku csv</div>';
}
?>
</table>
</div>
<script type="text/javascript">
var ctx = document.getElementById('myChart').getContext('2d');
var countries = [];
var peoples = [];

    $.getJSON("getData.php",function(data) {
        for(var i=0;i<data.length;i++) { 
            countries[i] = data[i].country;
            peoples[i] = data[i].qty;
        }
        buildChart();
});

function buildChart() {  
var chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: countries,
        datasets: [{
            label: "Countries",
            backgroundColor: '#007bff',
            borderColor: '#007bff',
            data: peoples,
        }]
    },
    options: {}
});
}
</script>
</body>
</html>