<?php

require 'vendor/autoload.php';

/*
 *  Pobieranie danych z zewnętrznego csv zapisanie do bazy i wyswietlanie wykresu.
 *
 *  @author Michał Jendraszczyk
 *  @copyright 2019
 */

use League\Csv\Reader;
use League\Csv\Statement;

class Import
{
    private $reader;
    public $records;
    public $connect;

    public function __construct()
    {
        try {
            $this->connect = new PDO('mysql:host=192.168.64.2;dbname=import', 'mjendraszczyk', '1');
            //$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Problem z polaczeniem <br/>'.$e->getMessage();
        }
    }

    public function createDatabase()
    {
        $pdo = $this->connect;
        $sql = $pdo->prepare('CREATE TABLE `import`.`import` ( `id` INT(16) UNSIGNED NOT NULL AUTO_INCREMENT , `name` VARCHAR(75) NOT NULL , `email` VARCHAR(100) NOT NULL , `country` VARCHAR(100) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;');
        $sql->execute();
    }

    public function uploadCSV($file)
    {
        $dir = 'assets/';
        //return print_r($file);
        if ($file['filename']['type'] == 'text/csv') {
            //  return print_r($file);

            if (move_uploaded_file($_FILES['filename']['tmp_name'], $dir.'import.csv')) {
                //return  'Plik '.basename($_FILES['filename']['name']).' przeslany pomyslnie';
                return header('Location:index.php');
            } else {
                return 'Problem podczas uploadowania pliku';
            }
        } else {
            echo 'Niepoprawny format';
        }
    }

    public function getCSV()
    {
        $this->createDatabase();

        $this->reader = Reader::createFromPath('assets/import.csv', 'r');
        $this->reader->setDelimiter(',');
        $this->reader->setHeaderOffset(0);

        $this->records = (new Statement())->process($this->reader);
        $this->records->getHeader();

        return $this->records;
    }

    public function setData()
    {
        $this->getCSV();

        foreach ($this->records as $record) {
            $QtyRows = $this->connect->query('SELECT COUNT(*) FROM `import` WHERE `email` = "'.$record['email'].'"')->fetchColumn();

            if ($QtyRows == 0) {
                $insert = $this->connect->prepare('INSERT INTO `import` (`name`, `email`, `country`)	VALUES(
            	:imie,
            	:email,
            	:kraj)');

                $insert->bindValue(':imie', $record['first_name'], PDO::PARAM_STR);
                $insert->bindValue(':email', $record['email'], PDO::PARAM_STR);
                $insert->bindValue(':kraj', $record['gender'], PDO::PARAM_STR);

                $insert->execute();
            }
        }
    }

    public function getData()
    {
        $this->setData();

        $getPersons = $this->connect->query('SELECT COUNT(id) as qty, country FROM import GROUP BY country');

        return json_encode($getPersons->fetchAll(PDO::FETCH_ASSOC));
    }
}
